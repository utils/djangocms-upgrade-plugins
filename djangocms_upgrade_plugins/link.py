"""
Old:
{
  "model": "cms.cmsplugin",
  "pk": 752,
  "fields": {
    "path": "0015000100020001",
    "depth": 4,
    "numchild": 2,
    "placeholder": 11,
    "parent": 751,
    "position": 1,
    "language": "cs",
    "plugin_type": "Bootstrap4LinkPlugin",
    "creation_date": "2023-08-09T06:49:10.996Z",
    "changed_date": "2023-08-09T07:02:29.492Z"
  }
}
New:
{
  "model": "cms.cmsplugin",
  "pk": 7,
  "fields": {
    "path": "0009",
    "depth": 1,
    "numchild": 0,
    "placeholder": 10,
    "parent": null,
    "position": 3,
    "language": "cs",
    "plugin_type": "LinkPlugin",
    "creation_date": "2023-08-14T14:10:42.206Z",
    "changed_date": "2023-08-14T14:10:42.238Z"
  }
}

Old:
{
  "model": "bootstrap4_link.bootstrap4link",
  "pk": 752,
  "fields": {
    "template": "default",
    "name": "info@csnog.eu",
    "external_link": "",
    "internal_link": null,
    "file_link": null,
    "anchor": "",
    "mailto": "info@csnog.eu",
    "phone": "",
    "target": "",
    "attributes": {
      "class": "text-white d-flex align-items-center justify-content-center mb-n-0 "
    },
    "cmsplugin_ptr": 752,
    "link_type": "link",
    "link_context": "link",
    "link_size": "",
    "link_outline": false,
    "link_block": false,
    "icon_left": "",
    "icon_right": ""
  }
}
New:
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 7,
  "fields": {
    "cmsplugin_ptr": 7,
    "ui_item": "Link",
    "tag_type": "div",
    "config": {
      "name": "Odkaz",
      "template": "default",
      "link_type": "link",
      "link_context": "",
      "link_size": "",
      "link_outline": false,
      "link_block": false,
      "icon_left": "",
      "icon_right": "",
      "link_stretched": false,
      "attributes": {},
      "margin_x": "",
      "margin_y": "",
      "margin_devices": null,
      "padding_x": "",
      "padding_y": "",
      "padding_devices": null,
      "external_link": "https://www.nic.cz",
      "internal_link": "",
      "internal_link": {
        "model": "cms.page",
        "pk": 1
      },
      "file_link": null,
      "anchor": "",
      "mailto": "",
      "phone": "",
      "target": ""
    }
  }
}
"""
from .utils import DUMP_UNKNOWN_FIELDS, DataDict, addInternalLink, debugFields, fillConfig


def bootstrap4link(item: DataDict) -> DataDict:
    """Convert bootstrap4link."""
    field_names = (
        "template",
        "name",
        "external_link",
        "internal_link",
        "file_link",
        "anchor",
        "mailto",
        "phone",
        "target",
        "attributes",
        "link_context",
        "link_size",
        "link_outline",
        "link_block",
        "link_target",
        "link_type",
    )
    debugFields(item, field_names, disable=not DUMP_UNKNOWN_FIELDS)
    new_item = {
        "model": "djangocms_frontend.frontenduiitem",
        "pk": item['pk'],
        "fields": {
            "cmsplugin_ptr": item['pk'],
            "ui_item": "Link",
            "tag_type": "div",
        }
    }
    required_fields = {
      "template": "default",
      "link_type": "link",
      "link_context": "",
      "link_size": "",
      "link_outline": False,
      "link_block": False,
      "icon_left": "",
      "icon_right": "",
      "link_stretched": False,
      "attributes": {},
      "margin_x": "",
      "margin_y": "",
      "margin_devices": None,
      "padding_x": "",
      "padding_y": "",
      "padding_devices": None,
      "external_link": "",
      "internal_link": "",
      "file_link": None,
      "anchor": "",
      "mailto": "",
      "phone": "",
      "target": ""
    }
    new_item['fields']['config'] = fillConfig(item, field_names, required_fields, {"link_target": "target"})
    addInternalLink(item, new_item)
    return new_item
