"""
Grid

Old:
{
  "model": "cms.cmsplugin",
  "pk": 955,
  "fields": {
    "path": "001G0001",
    "depth": 2,
    "numchild": 1,
    "placeholder": 22,
    "parent": 954,
    "position": 0,
    "language": "cs",
    "plugin_type": "Bootstrap4GridContainerPlugin",
    "creation_date": "2023-08-03T12:38:38.655Z",
    "changed_date": "2023-08-09T07:23:54.232Z"
  }
}
New:
{
  "model": "cms.cmsplugin",
  "pk": 2,
  "fields": {
    "path": "0007",
    "depth": 1,
    "numchild": 1,
    "placeholder": 10,
    "parent": null,
    "position": 1,
    "language": "cs",
    "plugin_type": "GridContainerPlugin",
    "creation_date": "2023-08-14T14:08:05.757Z",
    "changed_date": "2023-08-14T14:08:05.761Z"
  }
}

Old:
{
  "model": "bootstrap4_grid.bootstrap4gridcontainer",
  "pk": 955,
  "fields": {
    "cmsplugin_ptr": 955,
    "container_type": "container",
    "tag_type": "div",
    "attributes": {}
  }
}
New:
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 2,
  "fields": {
    "cmsplugin_ptr": 2,
    "ui_item": "GridContainer",
    "tag_type": "div",
    "config": {
      "container_type": "container",
      "attributes": {},
      "plugin_title": {
        "show": false,
        "title": ""
      },
      "background_context": "",
      "background_opacity": "100",
      "background_shadow": "",
      "responsive_visibility": null,
      "margin_x": "",
      "margin_y": "",
      "margin_devices": null,
      "padding_x": "",
      "padding_y": "",
      "padding_devices": null,
      "size_x": "",
      "size_y": ""
    }
  }
}

Grid row:

Old:
{
  "model": "cms.cmsplugin",
  "pk": 956,
  "fields": {
    "path": "001G00010001",
    "depth": 3,
    "numchild": 2,
    "placeholder": 22,
    "parent": 955,
    "position": 0,
    "language": "cs",
    "plugin_type": "Bootstrap4GridRowPlugin",
    "creation_date": "2023-08-03T12:38:47.096Z",
    "changed_date": "2023-08-09T07:23:54.235Z"
  }
}
{
  "model": "bootstrap4_grid.bootstrap4gridrow",
  "pk": 956,
  "fields": {
    "cmsplugin_ptr": 956,
    "vertical_alignment": "",
    "horizontal_alignment": "",
    "gutters": false,
    "tag_type": "div",
    "attributes": {
      "class": "pt-7 pt-md-10 pb-10 pb-md-11 "
    }
  }
}
New:
{
  "model": "cms.cmsplugin",
  "pk": 3,
  "fields": {
    "path": "00070001",
    "depth": 2,
    "numchild": 1,
    "placeholder": 10,
    "parent": 2,
    "position": 0,
    "language": "cs",
    "plugin_type": "GridRowPlugin",
    "creation_date": "2023-08-14T14:08:25.609Z",
    "changed_date": "2023-08-14T14:08:25.613Z"
  }
}
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 3,
  "fields": {
    "cmsplugin_ptr": 3,
    "ui_item": "GridRow",
    "tag_type": "div",
    "config": {
      "vertical_alignment": "",
      "horizontal_alignment": "",
      "gutters": "",
      "attributes": {
        "class": "pt-7 pt-md-10 pb-10 pb-md-11"
      },
      "row_cols_xs": null,
      "row_cols_sm": null,
      "row_cols_md": null,
      "row_cols_lg": null,
      "row_cols_xl": null,
      "row_cols_xxl": null,
      "plugin_title": {
        "show": false,
        "title": ""
      },
      "responsive_visibility": null,
      "margin_x": "",
      "margin_y": "",
      "margin_devices": null,
      "padding_x": "",
      "padding_y": "",
      "padding_devices": null
    }
  }
}

Column:
Old:
{
  "model": "bootstrap4_grid.bootstrap4gridcolumn",
  "pk": 748,
  "fields": {
    "cmsplugin_ptr": 748,
    "column_type": "col",
    "column_alignment": "",
    "tag_type": "div",
    "attributes": {
      "class": "text-white"
    },
    "xs_col": 12,
    "xs_order": null,
    "xs_offset": null,
    "xs_ml": false,
    "xs_mr": false,
    "sm_col": null,
    "sm_order": null,
    "sm_offset": null,
    "sm_ml": false,
    "sm_mr": false,
    "md_col": null,
    "md_order": null,
    "md_offset": null,
    "md_ml": false,
    "md_mr": false,
    "lg_col": null,
    "lg_order": null,
    "lg_offset": null,
    "lg_ml": false,
    "lg_mr": false,
    "xl_col": null,
    "xl_order": null,
    "xl_offset": null,
    "xl_ml": false,
    "xl_mr": false
  }
}
New:
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 4,
  "fields": {
    "cmsplugin_ptr": 4,
    "ui_item": "GridColumn",
    "tag_type": "div",
    "config": {
      "column_alignment": "",
      "text_alignment": "",
      "attributes": {},
      "xs_col": null,
      "xs_order": null,
      "xs_offset": null,
      "xs_ms": false,
      "xs_me": false,
      "sm_col": null,
      "sm_order": null,
      "sm_offset": null,
      "sm_ms": false,
      "sm_me": false,
      "md_col": null,
      "md_order": null,
      "md_offset": null,
      "md_ms": false,
      "md_me": false,
      "lg_col": null,
      "lg_order": null,
      "lg_offset": null,
      "lg_ms": false,
      "lg_me": false,
      "xl_col": null,
      "xl_order": null,
      "xl_offset": null,
      "xl_ms": false,
      "xl_me": false,
      "xxl_col": null,
      "xxl_order": null,
      "xxl_offset": null,
      "xxl_ms": false,
      "xxl_me": false,
      "plugin_title": {
        "show": false,
        "title": ""
      },
      "background_context": "",
      "background_opacity": "100",
      "background_shadow": "",
      "responsive_visibility": null,
      "margin_x": "",
      "margin_y": "",
      "margin_devices": null,
      "padding_x": "",
      "padding_y": "",
      "padding_devices": null
    }
  }
}
"""
from .utils import DUMP_UNKNOWN_FIELDS, DataDict, debugFields, setItemAndSpaces


def grid_container(item: DataDict) -> DataDict:
    """Convert grid container."""
    debugFields(item, ['container_type', "tag_type"], disable=not DUMP_UNKNOWN_FIELDS)
    required_fields: DataDict = {
        "container_type": "container",
        "background_context": "",
        "background_opacity": "100",
        "background_shadow": "",
        "responsive_visibility": None,
        "margin_x": "",
        "margin_y": "",
        "margin_devices": None,
        "padding_x": "",
        "padding_y": "",
        "padding_devices": None,
        "size_x": "",
        "size_y": "",
        "attributes": {},
        "plugin_title": {
          "show": False,
          "title": ""
        },
    }
    return setItemAndSpaces(item, "GridContainer", ("container_type", "attributes"), required_fields)


def grid_row_container(item: DataDict) -> DataDict:
    """Convert grid row."""
    extra_attrs = ["tag_type", "attributes", 'vertical_alignment', 'horizontal_alignment']
    debugFields(item, extra_attrs, disable=not DUMP_UNKNOWN_FIELDS)
    required_fields = {
        "vertical_alignment": "",
        "horizontal_alignment": "",
        "gutters": "",
        "attributes": {},
        "row_cols_xs": None,
        "row_cols_sm": None,
        "row_cols_md": None,
        "row_cols_lg": None,
        "row_cols_xl": None,
        "row_cols_xxl": None,
        "plugin_title": {
            "show": False,
            "title": ""
        },
        "responsive_visibility": None,
        "margin_x": "",
        "margin_y": "",
        "margin_devices": None,
        "padding_x": "",
        "padding_y": "",
        "padding_devices": None,
    }
    field_names = (
        "vertical_alignment",
        "horizontal_alignment",
        "gutters",
        "tag_type",
    )
    new_item = setItemAndSpaces(item, "GridRow", field_names + ("attributes", ), required_fields)
    for name in ("vertical_alignment", "horizontal_alignment", "gutters"):
        if name in item['fields']:
            new_item['fields']['config'][name] = item['fields'][name]
    return new_item


def grid_column_container(item: DataDict) -> DataDict:
    """Convert grid column."""
    field_names = (
        "column_alignment",
        "xs_col",
        "xs_order",
        "xs_offset",
        "xs_ml",
        "xs_mr",
        "sm_col",
        "sm_order",
        "sm_offset",
        "sm_ml",
        "sm_mr",
        "md_col",
        "md_order",
        "md_offset",
        "md_ml",
        "md_mr",
        "lg_col",
        "lg_order",
        "lg_offset",
        "lg_ml",
        "lg_mr",
        "xl_col",
        "xl_order",
        "xl_offset",
        "xl_ml",
        "xl_mr",
    )
    extra_fields = (
        'column_type',
        'tag_type',
        'attributes',
    )
    debugFields(item, field_names + extra_fields, disable=not DUMP_UNKNOWN_FIELDS)
    required_fields = {
        "column_alignment": "",
        "text_alignment": "",
        "attributes": {},
        "xs_col": None,
        "xs_order": None,
        "xs_offset": None,
        "xs_ms": False,
        "xs_me": False,
        "sm_col": None,
        "sm_order": None,
        "sm_offset": None,
        "sm_ms": False,
        "sm_me": False,
        "md_col": None,
        "md_order": None,
        "md_offset": None,
        "md_ms": False,
        "md_me": False,
        "lg_col": None,
        "lg_order": None,
        "lg_offset": None,
        "lg_ms": False,
        "lg_me": False,
        "xl_col": None,
        "xl_order": None,
        "xl_offset": None,
        "xl_ms": False,
        "xl_me": False,
        "xxl_col": None,
        "xxl_order": None,
        "xxl_offset": None,
        "xxl_ms": False,
        "xxl_me": False,
        "plugin_title": {
            "show": False,
            "title": ""
        },
        "background_context": "",
        "background_opacity": "100",
        "background_shadow": "",
        "responsive_visibility": None,
        "margin_x": "",
        "margin_y": "",
        "margin_devices": None,
        "padding_x": "",
        "padding_y": "",
        "padding_devices": None
    }
    return setItemAndSpaces(item, "GridColumn", field_names + ("attributes", ), required_fields)
