"""
Old:
{
  "model": "filer.folder",
  "pk": 1,
  "fields": {
    "level": 0,
    "lft": 1,
    "rght": 2,
    "parent": null,
    "name": "icon",
    "owner": [
      "admin"
    ],
    "uploaded_at": "2023-08-04T11:56:18.341Z",
    "created_at": "2023-08-04T11:56:18.341Z",
    "modified_at": "2023-08-04T11:56:18.341Z",
    "tree_id": 1
  }
}
New:
{
  "model": "filer.folder",
  "pk": 1,
  "fields": {
    "parent": null,
    "name": "icons",
    "owner": [
      "admin"
    ],
    "uploaded_at": "2023-08-18T14:10:03.223Z",
    "created_at": "2023-08-18T14:10:03.223Z",
    "modified_at": "2023-08-18T14:10:03.223Z"
  }
}

File:
{
  "model": "filer.file",
  "pk": 53,
  "fields": {
    "polymorphic_ctype": [
      "filer",
      "image"
    ],
    "folder": 1,
    "file": "filer_public/a4/7e/a47e5f63-e89f-42c4-abcd-b726f839e387/iconfinder_python_logo_282803.svg",
    "_file_size": 1612,
    "sha1": "8687447bd187ceccf04b96c72a3340dfc918cb8f",
    "has_all_mandatory_data": false,
    "original_filename": "iconfinder_Python_logo_282803.svg",
    "name": "",
    "description": null,
    "owner": [
      "admin"
    ],
    "uploaded_at": "2023-08-20T11:11:17.138Z",
    "modified_at": "2023-08-20T11:11:17.138Z",
    "is_public": true,
    "mime_type": "image/svg+xml"
  }
}

"""
import os
from copy import deepcopy
from typing import Optional, Tuple

import easy_thumbnails.utils
from easy_thumbnails.VIL import Image as VILImage
from filer.utils.compatibility import PILImage

from .utils import DUMP_UNKNOWN_FIELDS, DataDict, debugFields, fillConfig

MEDIA_ROOT = 'media'
IMAGE_TYPES = {
    '.svg': 'image/svg+xml',
    '.png': 'image/png',
    '.jpg': 'image/jpeg',
    '.jpeg': 'image/jpeg',
    '.gif': 'image/gif',
}


def filerFolder(item: DataDict) -> DataDict:
    """Convert filer folder."""
    field_names = (
        "parent",
        "name",
        "owner",
        'uploaded_at',
        'created_at',
        'modified_at',
    )
    skip_fields = (
        'level',
        'lft',
        'rght',
        'tree_id',
    )
    debugFields(item, field_names + skip_fields, disable=not DUMP_UNKNOWN_FIELDS)
    new_item = item.copy()
    new_item.pop("fields")
    new_item["fields"] = fillConfig(item, field_names)
    return new_item


def getImageMimeType(filename: str) -> str:
    """Get mime type."""
    return IMAGE_TYPES.get(filename[-4:], "")


def filerFile(item: DataDict) -> DataDict:
    """Convert filer file."""
    new_item = deepcopy(item)
    mime_type = getImageMimeType(item["fields"]["original_filename"])
    if mime_type:
        new_item["fields"]["polymorphic_ctype"] = ["filer", "image"]
        new_item["fields"]["mime_type"] = mime_type
    return new_item


def getFileWidthHeight(path: str) -> Tuple[Optional[float], Optional[float], bool]:
    """Get File Width Height."""
    mime_type = getImageMimeType(path)
    if mime_type == 'image/svg+xml':
        image = VILImage.load(path)
        if image is None:
            print(f"Path {path} not found!")
            width, height = 300, 200
        else:
            width, height = image.size
        transparent = True
    else:
        pil_image = PILImage.open(path)
        width, height = pil_image.size
        transparent = easy_thumbnails.utils.is_transparent(pil_image)
    return width, height, transparent


def createImage(item: DataDict, image_id: int, file_ptr: int, media_root: Optional[str]) -> DataDict:
    """Create image."""
    if media_root is None:
        media_root = MEDIA_ROOT
    path = os.path.join(media_root, item['fields']['file'])
    width, height, transparent = getFileWidthHeight(path)
    return {
        "model": "filer.image",
        "pk": image_id,
        "fields": {
            "_height": height,
            "_width": width,
            "_transparent": transparent,
            "default_alt_text": None,
            "default_caption": None,
            "subject_location": "",
            "file_ptr": file_ptr,
            "date_taken": item["fields"]["uploaded_at"],
            "author": None,
            "must_always_publish_author_credit": False,
            "must_always_publish_copyright": False
        }
    }
