"""
Old:
{
  "model": "bootstrap4_jumbotron.bootstrap4jumbotron",
  "pk": 3012,
  "fields": {
    "cmsplugin_ptr": 3012,
    "fluid": false,
    "tag_type": "div",
    "attributes": {}
  }
}

New:
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 10,
  "fields": {
    "cmsplugin_ptr": 10,
    "ui_item": "Jumbotron",
    "tag_type": "div",
    "config": {
      "jumbotron_fluid": false,
      "template": "default",
      "attributes": {},
      "responsive_visibility": null,
      "margin_x": "",
      "margin_y": "",
      "margin_devices": null,
      "padding_x": "",
      "padding_y": "",
      "padding_devices": null,
      "background_context": "",
      "background_opacity": "100",
      "background_shadow": ""
    }
  }
}
"""
from .utils import DUMP_UNKNOWN_FIELDS, DataDict, debugFields, setItem


def bootstrap4jumbotron(item: DataDict) -> DataDict:
    """Convert Bootstrap 4 Jumbotron."""
    field_names = (
        "tag_type",
        "fluid",
        "attributes",
    )
    debugFields(item, field_names, disable=not DUMP_UNKNOWN_FIELDS)
    required_fields = {
      "jumbotron_fluid": False,
      "template": "default",
      "attributes": {},
      "responsive_visibility": None,
      "margin_x": "",
      "margin_y": "",
      "margin_devices": None,
      "padding_x": "",
      "padding_y": "",
      "padding_devices": None,
      "background_context": "",
      "background_opacity": "100",
      "background_shadow": ""
    }
    return setItem(item, "Jumbotron", field_names, required_fields, {'fluid': 'jumbotron_fluid'})
