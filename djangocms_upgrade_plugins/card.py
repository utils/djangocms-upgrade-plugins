"""
Old:
{
  "model": "bootstrap4_card.bootstrap4card",
  "pk": 3007,
  "fields": {
    "cmsplugin_ptr": 3007,
    "card_type": "card",
    "card_context": "primary",
    "card_alignment": "",
    "card_outline": false,
    "card_text_color": "white",
    "tag_type": "div",
    "attributes": {}
  }
}
{
  "model": "bootstrap4_card.bootstrap4cardinner",
  "pk": 3008,
  "fields": {
    "cmsplugin_ptr": 3008,
    "inner_type": "card-body",
    "tag_type": "div",
    "attributes": {}
  }
}

New:
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 5,
  "fields": {
    "cmsplugin_ptr": 5,
    "ui_item": "Card",
    "tag_type": "div",
    "config": {
      "card_alignment": "",
      "card_outline": "",
      "card_text_color": "",
      "card_full_height": false,
      "attributes": {},
      "background_context": "",
      "background_opacity": "100",
      "background_shadow": "",
      "responsive_visibility": null,
      "margin_x": "",
      "margin_y": "",
      "margin_devices": null
    }
  }
},
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 6,
  "fields": {
    "cmsplugin_ptr": 6,
    "ui_item": "CardInner",
    "tag_type": "div",
    "config": {
      "inner_type": "card-body"
    }
  }
}
"""
from .utils import DUMP_UNKNOWN_FIELDS, DataDict, debugFields, setItem


def bootstrap4card(item: DataDict) -> DataDict:
    """Convert Bootstrap 4 Card."""
    field_names = (
        "tag_type",
        "card_type",
        "card_context",
        "card_alignment",
        "card_outline",
        "card_text_color",
        "attributes"
    )
    debugFields(item, field_names, disable=not DUMP_UNKNOWN_FIELDS)
    required_fields = {
      "card_alignment": "",
      "card_outline": "",
      "card_text_color": "",
      "card_full_height": False,
      "attributes": {},
      "background_context": "",
      "background_opacity": "100",
      "background_shadow": "",
      "responsive_visibility": None,
      "margin_x": "",
      "margin_y": "",
      "margin_devices": None,
    }
    return setItem(item, "Card", field_names, required_fields)


def bootstrap4cardinner(item: DataDict) -> DataDict:
    """Convert Bootstrap 4 Card Inner."""
    field_names = (
        "tag_type",
        "inner_type",
        "attributes"
    )
    debugFields(item, field_names, disable=not DUMP_UNKNOWN_FIELDS)
    required_fields = {
      "inner_type": "card-body",
      "attributes": {}
    }
    return setItem(item, "CardInner", field_names, required_fields)
