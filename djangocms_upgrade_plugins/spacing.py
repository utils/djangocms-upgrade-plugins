"""
Old:
{
  "model": "bootstrap4_utilities.bootstrap4spacing",
  "pk": 3016,
  "fields": {
    "cmsplugin_ptr": 3016,
    "space_property": "m",
    "space_sides": "",
    "space_size": "0",
    "space_device": "",
    "tag_type": "div",
    "attributes": {}
  }
}

New:
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 11,
  "fields": {
    "cmsplugin_ptr": 11,
    "ui_item": "Spacing",
    "tag_type": "div",
    "config": {
      "space_property": "m",
      "space_sides": "",
      "space_size": "0",
      "space_device": "",
      "attributes": {}
    }
  }
}
"""
from .utils import DUMP_UNKNOWN_FIELDS, DataDict, debugFields, setItem


def bootstrap4spacing(item: DataDict) -> DataDict:
    """Convert Bootstrap 4 Spacing."""
    field_names = (
        "tag_type",
        "space_property",
        "space_sides",
        "space_size",
        "space_device",
        "attributes",
    )
    debugFields(item, field_names, disable=not DUMP_UNKNOWN_FIELDS)
    required_fields = {
      "space_property": "m",
      "space_sides": "",
      "space_size": "0",
      "space_device": "",
      "attributes": {}
    }
    return setItem(item, "Spacing", field_names, required_fields)
