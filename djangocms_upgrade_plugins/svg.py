"""
Old:
{
    "model": "cms.cmsplugin",
    "pk": 1319,
    "fields": {
        "path": "001O0002",
        "depth": 2,
        "numchild": 0,
        "placeholder": 22,
        "parent": 1305,
        "position": 0,
        "language": "cs",
        "plugin_type": "SvgImagePlugin",
        "creation_date": "2023-08-08T08:34:51.442Z",
        "changed_date": "2023-08-09T12:39:19.694Z"
    }
}
New:
{
    "model": "cms.cmsplugin",
    "pk": 11,
    "fields": {
        "path": "0007000100010001",
        "depth": 4,
        "numchild": 0,
        "placeholder": 10,
        "parent": 4,
        "position": 0,
        "language": "cs",
        "plugin_type": "ImagePlugin",
        "creation_date": "2023-08-17T12:34:33.538Z",
        "changed_date": "2023-08-17T12:34:33.544Z"
      }
}

Old:
{
    "model": "cmsplugin_svg.svgimage",
    "pk": 1319,
    "fields": {
        "cmsplugin_ptr": 1319,
        "label": "",
        "id_name": "",
        "tag_type": "",
        "svg_image": 37,
        "additional_class_names": "",
        "alignment": null,
        "width": null,
        "height": null,
        "caption_text": null,
        "alt_text": null
    }
}
{
  "model": "cmsplugin_svg.svgimage",
  "pk": 786,
  "fields": {
    "cmsplugin_ptr": 786,
    "label": "My label",
    "id_name": "my-id-variable",
    "tag_type": "figure",
    "svg_image": 1,
    "additional_class_names": "extra-class",
    "alignment": null,
    "width": null,
    "height": null,
    "caption_text": "Caption logo.",
    "alt_text": "Alt text."
  }
}

New:
{
    "model": "djangocms_frontend.frontenduiitem",
    "pk": 11,
    "fields": {
        "cmsplugin_ptr": 11,
        "ui_item": "Image",
        "tag_type": "div",
        "config": {
            "template": "default",
            "picture": {
                "model": "filer.image",
                "pk": 2
            },
            "external_picture": "",
            "lazy_loading": false,
            "width": null,
            "height": null,
            "alignment": "start",
            "link_attributes": {},
            "use_automatic_scaling": false,
            "use_crop": false,
            "use_no_cropping": false,
            "use_upscale": false,
            "use_responsive_image": "inherit",
            "thumbnail_options": null,
            "picture_fluid": true,
            "picture_rounded": false,
            "picture_thumbnail": false,
            "attributes": {},
            "external_link": "",
            "internal_link": "",
            "file_link": null,
            "anchor": "",
            "mailto": "",
            "phone": "",
            "target": "",
            "responsive_visibility": null,
            "margin_x": "",
            "margin_y": "",
            "margin_devices": null
        }
    }
}
"""
from copy import deepcopy
from typing import Optional

from .utils import (DUMP_UNKNOWN_FIELDS, DataDict, addAttribute, addInternalLink, debugFields, fillPictureConfig,
                    setAlignment)


def svg_image(item: DataDict) -> DataDict:
    """Convert svgimage."""
    field_names = ('width', 'height', 'alignment')
    skip_names = (
        'svg_image',
        'alt_text',
        'label',
        'id_name',
        'tag_type',
        'additional_class_names',
        'caption_text',
    )
    debugFields(item, field_names + skip_names, disable=not DUMP_UNKNOWN_FIELDS)
    new_item = {
        "model": "djangocms_frontend.frontenduiitem",
        "pk": item['pk'],
        "fields": {
            "cmsplugin_ptr": item['pk'],
            "ui_item": "Image",
            "tag_type": "div",
        }
    }
    new_item['fields']['config'] = fillPictureConfig(item, field_names, 'svg_image')
    addInternalLink(item, new_item)
    addAttribute(item, new_item, 'alt_text', 'alt')
    setAlignment(item, new_item)  # Convert left and right to new values start and end.
    return new_item


def prepare_for_style_figure(
    item: DataDict, cms_plugin_id: int, tag_type: Optional[str] = None
) -> DataDict:
    """Prepare data for style."""
    figure = deepcopy(item)
    figure['pk'] = cms_plugin_id
    figure['fields']['class_name'] = None
    figure['fields']['attributes'] = {}
    for name in ('svg_image', 'width', 'height', 'caption_text', 'alt_text'):
        figure['fields'].pop(name, None)
    if tag_type is not None:
        figure['fields']['tag_type'] = tag_type
    return figure
