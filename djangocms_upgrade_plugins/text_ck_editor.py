"""
{
    "model": "djangocms_text_ckeditor.text",
    "pk": 3,
    "fields": {
        "cmsplugin_ptr": 3,
        "body": "<p>....</p>"
    }
},
"""
from .utils import DataDict


def addTextCKEditorPlugin(cms_plugin_id: int, body: str) -> DataDict:
    """Add Text CK-Editor Plugin."""
    return {
        "model": "djangocms_text_ckeditor.text",
        "pk": cms_plugin_id,
        "fields": {
            "cmsplugin_ptr": cms_plugin_id,
            "body": body,
        }
    }
