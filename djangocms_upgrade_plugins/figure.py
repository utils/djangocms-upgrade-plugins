"""
Old:
{
  "model": "bootstrap4_content.bootstrap4figure",
  "pk": 3011,
  "fields": {
    "cmsplugin_ptr": 3011,
    "figure_caption": "Figure Caption",
    "figure_alignment": "text-left",
    "attributes": {}
  }
}

New:
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 9,
  "fields": {
    "cmsplugin_ptr": 9,
    "ui_item": "Figure",
    "tag_type": "div",
    "config": {
      "figure_caption": "<p>Figure text.</p>",
      "figure_alignment": "",
      "attributes": {},
      "margin_x": "",
      "margin_y": "",
      "margin_devices": null,
      "padding_x": "",
      "padding_y": "",
      "padding_devices": null,
      "responsive_visibility": null,
      "background_context": "",
      "background_opacity": "100",
      "background_shadow": ""
    }
  }
}
"""
from .utils import DUMP_UNKNOWN_FIELDS, DataDict, debugFields, setItem


def bootstrap4figure(item: DataDict) -> DataDict:
    """Convert Bootstrap 4 Figure."""
    field_names = (
        "figure_caption",
        "figure_alignment",
        "attributes",
    )
    debugFields(item, field_names, disable=not DUMP_UNKNOWN_FIELDS)
    required_fields = {
      "figure_caption": "",
      "figure_alignment": "",
      "attributes": {},
      "margin_x": "",
      "margin_y": "",
      "margin_devices": None,
      "padding_x": "",
      "padding_y": "",
      "padding_devices": None,
      "responsive_visibility": None,
      "background_context": "",
      "background_opacity": "100",
      "background_shadow": ""
    }
    new_item = setItem(item, "Figure", field_names, required_fields)
    config = new_item["fields"]["config"]
    config["figure_caption"] = f"<p>{config['figure_caption']}</p>"
    return new_item
