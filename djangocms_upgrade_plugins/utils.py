from copy import deepcopy
from typing import Any, Callable, Dict, Optional, Sequence, Tuple

DUMP_UNKNOWN_FIELDS = True

DataDict = Dict[str, Any]


def fillConfig(
        item: DataDict,
        field_names: Sequence[str],
        required_fields: Optional[DataDict] = None,
        renamed_fields: Optional[Dict[str, str]] = None  # {old: new}
) -> DataDict:
    """Fill config."""
    config = {}
    if renamed_fields is None:
        renamed_fields = {}
    for name in field_names:
        if item['fields'].get(name):
            config[renamed_fields.get(name, name)] = item['fields'][name]
    if required_fields is not None:
        for name, value in required_fields.items():
            if name not in config:
                config[name] = value
    return config


def fillPictureConfig(
    item: DataDict,
    field_names: Sequence[str],
    picture_name: str,
    renamed_fields: Optional[Dict[str, str]] = None
) -> DataDict:
    """Fill picture config."""
    use_no_cropping = picture_name == "svg_image"
    config = fillConfig(item, field_names, {
        "template": "default",
        "external_picture": "",
        "lazy_loading": False,
        "width": None,
        "height": None,
        "alignment": "start",
        "link_attributes": {},
        "use_automatic_scaling": False,
        "use_crop": False,
        "use_no_cropping": use_no_cropping,
        "use_upscale": False,
        "use_responsive_image": "inherit",
        "thumbnail_options": None,
        "picture_fluid": not use_no_cropping,
        "picture_rounded": False,
        "picture_thumbnail": False,
        "attributes": {},
        "external_link": "",
        "internal_link": "",
        "file_link": None,
        "anchor": "",
        "mailto": "",
        "phone": "",
        "target": "",
        "responsive_visibility": None,
        "margin_x": "",
        "margin_y": "",
        "margin_devices": None
    }, renamed_fields)
    config['picture'] = {
        "model": "filer.image",
        "pk": item['fields'][picture_name],
    }
    return config


def setItem(
    item: DataDict,
    ui_item: str,
    field_names: Sequence[str],
    required_fields: DataDict,
    renamed_fields: Optional[Dict[str, str]] = None
) -> DataDict:
    """Set plugin item."""
    new_item = {
        "model": "djangocms_frontend.frontenduiitem",
        "pk": item['pk'],
        "fields": {
            "cmsplugin_ptr": item['pk'],
            "ui_item": ui_item,
            "tag_type": item['fields'].get("tag_type", "div"),
        }
    }
    new_item['fields']['config'] = fillConfig(item, field_names, required_fields, renamed_fields)
    return new_item


def setItemAndSpaces(
    item: DataDict,
    ui_item: str,
    field_names: Sequence[str],
    required_fields: DataDict,
    renamed_fields: Optional[Dict[str, str]] = None
) -> DataDict:
    """Set plugin item and spaces."""
    new_item = setItem(item, ui_item, field_names, required_fields, renamed_fields)
    fields = item["fields"]
    config = new_item['fields']['config']
    config['margin_x'], config['margin_y'] = setSpaces(fields, 'margin')
    config['padding_x'], config['padding_y'] = setSpaces(fields, 'padding')
    return new_item


def addInternalLink(item: DataDict, new_item: DataDict) -> None:
    """Add internal link if exists."""
    if "internal_link" in item['fields'] and item['fields']["internal_link"]:
        new_item['fields']['config']["internal_link"] = {
          "model": "cms.page",
          "pk": item['fields']["internal_link"],
        }
    if "link_page" in item['fields'] and item['fields']["link_page"]:
        new_item['fields']['config']["internal_link"] = {
          "model": "cms.page",
          "pk": item['fields']["link_page"],
        }
    if "file_link" in item['fields'] and item['fields']["file_link"]:
        new_item['fields']['config']["file_link"] = {
          "model": "filer.file",
          "pk": item['fields']["file_link"],
        }


def addAttribute(
    item: DataDict,
    new_item: DataDict,
    attribute_name: str,
    new_attr_name: Optional[str] = None,
    normalize_fnc: Optional[Callable[[str], str]] = None
) -> None:
    """Add attribute into attributes."""
    if new_attr_name is None:
        new_attr_name = attribute_name
    fields = item["fields"]
    if attribute_name in fields and fields[attribute_name]:
        attributes = new_item['fields']['config'].get("attributes", {})
        if new_attr_name in attributes:
            attributes[new_attr_name] += " " + fields[attribute_name]
        else:
            attributes[new_attr_name] = fields[attribute_name]
        if normalize_fnc is not None:
            attributes[new_attr_name] = normalize_fnc(attributes[new_attr_name])
        new_item['fields']['config']["attributes"] = attributes


def debugFields(item: DataDict, skip_fields: Optional[Sequence[str]] = None, disable: bool = True) -> None:
    """Debug item fields."""
    if disable:
        return
    fields = {}
    for key, value in item['fields'].items():
        if key == 'cmsplugin_ptr':
            continue
        if skip_fields is not None and key in skip_fields:
            continue
        if value:
            fields[key] = value
    if fields:
        print(fields)
        print(item)
        print()


def getRange(value: int) -> str:
    """Get range of the value."""
    # * 0     0
    # * .25   1 >   0
    # * .5    2 =>  5
    # * 1     3 => 10
    # * 1.5   4 => 15
    # * 3     5 >= 20
    result = 1
    if value >= 20:
        result = 5
    elif value >= 15:
        result = 4
    elif value >= 10:
        result = 3
    elif value >= 5:
        result = 2
    return str(result)


def setXY(fields: DataDict, key: str) -> Tuple[Optional[str], Optional[str]]:
    """Set padding or spacing for X, Y."""
    space_x, space_y = None, None

    if f'{key}_x' in fields and fields[f'{key}_x']:
        space_x = 'px-' + getRange(fields[f'{key}_x'])
    if f'{key}_y' in fields and fields[f'{key}_y']:
        space_y = 'py-' + getRange(fields[f'{key}_y'])

    return space_x, space_y


def setSpaces(fields: DataDict, key: str) -> Tuple[Optional[str], Optional[str]]:
    """Set padding or margin."""
    if f'{key}_x' in fields or f'{key}_y' in fields:
        return setXY(fields, key)

    space_x, space_y = None, None
    if f'{key}_left' in fields and fields[f'{key}_left'] and f'{key}_right' in fields and fields[f'{key}_right']:
        space_x = 'px-' + getRange(max(fields[f'{key}_left'], fields[f'{key}_right']))
    elif f'{key}_left' in fields and fields[f'{key}_left']:
        space_x = 'ps-' + getRange(fields[f'{key}_left'])
    elif f'{key}_right' in fields and fields[f'{key}_right']:
        space_x = 'pe-' + getRange(fields[f'{key}_right'])

    if f'{key}_top' in fields and fields[f'{key}_top'] and f'{key}_bottom' in fields and fields[f'{key}_bottom']:
        space_y = 'py-' + getRange(max(fields[f'{key}_top'], fields[f'{key}_bottom']))
    elif f'{key}_top' in fields and fields[f'{key}_top']:
        space_y = 'pt-' + getRange(fields[f'{key}_top'])
    elif f'{key}_bottom' in fields and fields[f'{key}_bottom']:
        space_y = 'pb-' + getRange(fields[f'{key}_bottom'])

    return space_x, space_y


def addPluginType(
    item: DataDict,
    cms_plugin_id: int,
    plugin_type: str,
    chunk: Optional[str] = None,
    position: Optional[int] = None
) -> DataDict:
    """Add TextPlugin."""
    plugin = deepcopy(item)
    fields = plugin["fields"]
    plugin["pk"] = cms_plugin_id
    fields["path"] = fields["path"] + ("0001" if chunk is None else chunk)
    fields["depth"] = fields["depth"] + 1
    fields["numchild"] = 0
    fields["parent"] = item["pk"]
    fields["position"] = 0 if position is None else position
    fields["plugin_type"] = plugin_type
    item["fields"]["numchild"] += 1
    return plugin


def duplicatePlugin(item: DataDict, cms_plugin_id: int, plugin_type: str) -> DataDict:
    """Duplicate Plugin."""
    new_item = deepcopy(item)
    new_item["pk"] = cms_plugin_id
    new_item["fields"]["plugin_type"] = plugin_type
    return new_item


def setAlignment(item: DataDict, new_item: DataDict) -> None:
    """Set alignment. Convert left and right to new values start and end."""
    alignment = item['fields'].get('alignment')
    if alignment is not None:
        new_item['fields']['config']['alignment'] = {'left': 'start', 'right': 'end'}.get(alignment, alignment)
