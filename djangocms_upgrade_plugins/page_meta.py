"""
Old:
{
    "model": "djangocms_page_meta.pagemeta",
    "pk": 3,
    "fields": {
        "public_extension": 8,
        "extended_object": 9,
        "image": 4,
        "og_type": "",
        "og_author": null,
        "og_author_url": "",
        "og_author_fbid": "",
        "og_publisher": "",
        "og_app_id": "",
        "fb_pages": "",
        "twitter_author": "",
        "twitter_site": "",
        "twitter_type": "",
        "gplus_author": "",
        "gplus_type": ""
    }
}
New:
{
  "model": "djangocms_page_meta.pagemeta",
  "pk": 1,
  "fields": {
    "public_extension": null,
    "extended_object": 1,
    "image": null,
    "og_type": "",
    "og_author": null,
    "og_author_url": "",
    "og_author_fbid": "",
    "og_publisher": "",
    "og_app_id": "",
    "fb_pages": "",
    "twitter_author": "Tumpach",
    "twitter_site": "",
    "twitter_type": "",
    "schemaorg_type": "",
    "robots": "['noindex', 'nofollow', 'noimageindex']"
  }
}
"""
from copy import deepcopy

from .utils import DataDict


def page_meta(item: DataDict) -> DataDict:
    """Adjust page meta."""
    new_item = deepcopy(item)
    new_item["fields"].pop("gplus_author")
    new_item["fields"].pop("gplus_type")
    return new_item
