"""
Old:
{
    "model": "cms.cmsplugin",
    "pk": 1584,
    "fields": {
        "path": "001R0003000200010001",
        "depth": 5,
        "numchild": 10,
        "placeholder": 9,
        "parent": 1583,
        "position": 0,
        "language": "cs",
        "plugin_type": "Bootstrap4CollapsePlugin",
        "creation_date": "2023-05-12T09:55:48.996Z",
        "changed_date": "2023-05-12T13:22:24.415Z"
    }
},
{
    "model": "cms.cmsplugin",
    "pk": 1585,
    "fields": {
        "path": "001R00030002000100010001",
        "depth": 6,
        "numchild": 1,
        "placeholder": 9,
        "parent": 1584,
        "position": 7,
        "language": "cs",
        "plugin_type": "Bootstrap4CollapseContainerPlugin",
        "creation_date": "2023-05-12T09:56:06.980Z",
        "changed_date": "2023-07-25T13:08:49.138Z"
    }
},
{
    "model": "cms.cmsplugin",
    "pk": 1587,
    "fields": {
        "path": "001R00030002000100010002",
        "depth": 6,
        "numchild": 1,
        "placeholder": 9,
        "parent": 1584,
        "position": 0,
        "language": "cs",
        "plugin_type": "Bootstrap4CollapseTriggerPlugin",
        "creation_date": "2023-05-12T09:56:40.431Z",
        "changed_date": "2023-07-22T11:47:39.699Z"
    }
},

{
    "model": "bootstrap4_collapse.bootstrap4collapse",
    "pk": 1584,
    "fields": {
        "cmsplugin_ptr": 1584,
        "siblings": ".card",
        "tag_type": "article",
        "attributes": {}
    }
},
{
    "model": "bootstrap4_collapse.bootstrap4collapsecontainer",
    "pk": 1585,
    "fields": {
        "cmsplugin_ptr": 1585,
        "identifier": "num4",
        "tag_type": "aside",
        "attributes": {}
    }
},
{
    "model": "bootstrap4_collapse.bootstrap4collapsetrigger",
    "pk": 1587,
    "fields": {
        "cmsplugin_ptr": 1587,
        "identifier": "num1",
        "tag_type": "header",
        "attributes": {}
    }
},

New:
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 8,
  "fields": {
    "cmsplugin_ptr": 8,
    "ui_item": "Collapse",
    "tag_type": "div",
    "config": {
      "collapse_siblings": ".card",
      "attributes": {}
    }
  }
},
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 9,
  "fields": {
    "cmsplugin_ptr": 9,
    "ui_item": "CollapseContainer",
    "tag_type": "div",
    "config": {
      "container_identifier": "kolapser-1",
      "attributes": {}
    }
  }
},
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 5,
  "fields": {
    "cmsplugin_ptr": 5,
    "ui_item": "CollapseTrigger",
    "tag_type": "div",
    "config": {
      "trigger_identifier": "",
      "attributes": {}
    }
  }
}
"""
from .utils import DUMP_UNKNOWN_FIELDS, DataDict, debugFields, setItem

TAG_TYPE_FIELD = ('tag_type', )


def bootstrap4collapse(item: DataDict) -> DataDict:
    """Convert bootstrap4collapse."""
    field_names = (
        "siblings",
        "attributes",
    )
    renamed_fields = {
        "siblings": "collapse_siblings",
    }
    debugFields(item, field_names + TAG_TYPE_FIELD, disable=not DUMP_UNKNOWN_FIELDS)
    required_fields = {
      "collapse_siblings": ".card",
      "attributes": {}
    }
    return setItem(item, "Collapse", field_names, required_fields, renamed_fields)


def bootstrap4collapse_container(item: DataDict) -> DataDict:
    """Convert bootstrap4collapsecontainer."""
    field_names = (
        "identifier",
        "attributes",
    )
    renamed_fields = {
        "identifier": "container_identifier",
    }
    debugFields(item, field_names + TAG_TYPE_FIELD, disable=not DUMP_UNKNOWN_FIELDS)
    required_fields: DataDict = {
        "attributes": {},
    }
    return setItem(item, "CollapseContainer", field_names, required_fields, renamed_fields)


def bootstrap4collapse_trigger(item: DataDict) -> DataDict:
    """Convert bootstrap4collapsetrigger."""
    field_names = (
        "identifier",
        "attributes",
    )
    renamed_fields = {
        "identifier": "trigger_identifier",
    }
    debugFields(item, field_names + TAG_TYPE_FIELD, disable=not DUMP_UNKNOWN_FIELDS)
    required_fields: DataDict = {
        "attributes": {},
    }
    return setItem(item, "CollapseTrigger", field_names, required_fields, renamed_fields)
