"""
Old:
{
  "model": "bootstrap4_content.bootstrap4code",
  "pk": 3010,
  "fields": {
    "cmsplugin_ptr": 3010,
    "code_content": "The plugin Code.",
    "tag_type": "code",
    "attributes": {}
  }
}

New:
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 8,
  "fields": {
    "cmsplugin_ptr": 8,
    "ui_item": "CodeBlock",
    "tag_type": "div",
    "config": {
      "code_content": "The plugin Code.",
      "code_type": "code",
      "attributes": {},
      "margin_x": "",
      "margin_y": "",
      "margin_devices": null,
      "padding_x": "",
      "padding_y": "",
      "padding_devices": null,
      "responsive_visibility": null,
      "background_context": "",
      "background_opacity": "100",
      "background_shadow": ""
    }
  }
},
"""
from .utils import DUMP_UNKNOWN_FIELDS, DataDict, debugFields, setItem


def bootstrap4code(item: DataDict) -> DataDict:
    """Convert Bootstrap 4 Code."""
    field_names = (
        "tag_type",
        "code_content",
        "attributes",
    )
    debugFields(item, field_names, disable=not DUMP_UNKNOWN_FIELDS)
    required_fields = {
      "code_content": "",
      "code_type": "code",
      "attributes": {},
      "margin_x": "",
      "margin_y": "",
      "margin_devices": None,
      "padding_x": "",
      "padding_y": "",
      "padding_devices": None,
      "responsive_visibility": None,
      "background_context": "",
      "background_opacity": "100",
      "background_shadow": ""
    }
    return setItem(item, "CodeBlock", field_names, required_fields)
