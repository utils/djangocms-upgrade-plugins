"""
Old:
{
    "model": "cms.cmsplugin",
    "pk": 3393,
    "fields": {
        "path": "00D6",
        "depth": 1,
        "numchild": 1,
        "placeholder": 225,
        "parent": null,
        "position": 0,
        "language": "en",
        "plugin_type": "Bootstrap4AlertsPlugin",
        "creation_date": "2020-09-17T12:11:09.214Z",
        "changed_date": "2020-09-17T12:12:23.582Z"
    }
}
New:
{
  "model": "cms.cmsplugin",
  "pk": 4176,
  "fields": {
    "path": "00FX",
    "depth": 1,
    "numchild": 1,
    "placeholder": 15,
    "parent": null,
    "position": 1,
    "language": "en",
    "plugin_type": "AlertPlugin",
    "creation_date": "2023-10-03T11:19:59.536Z",
    "changed_date": "2023-10-03T11:19:59.550Z"
  }
}

Old:
{
    "model": "bootstrap4_alerts.bootstrap4alerts",
    "pk": 3397,
    "fields": {
        "cmsplugin_ptr": 3397,
        "alert_context": "success",
        "alert_dismissable": false,
        "tag_type": "div",
        "attributes": {}
    }
}
New:
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 4176,
  "fields": {
    "cmsplugin_ptr": 4176,
    "ui_item": "Alert",
    "tag_type": "div",
    "config": {
      "alert_context": "primary",
      "alert_dismissible": false,
      "attributes": {},
      "alert_shadow": "",
      "responsive_visibility": null,
      "margin_x": "",
      "margin_y": "",
      "margin_devices": null,
      "padding_x": "",
      "padding_y": "",
      "padding_devices": null
    }
  }
}
"""
from .utils import DUMP_UNKNOWN_FIELDS, DataDict, debugFields, setItem


def bootstrap4alerts(item: DataDict) -> DataDict:
    """Convert Bootstrap 4 Alerts."""
    field_names = (
        "tag_type",
        "alert_context",
        'alert_dismissable',
        "attributes",
    )
    debugFields(item, field_names, disable=not DUMP_UNKNOWN_FIELDS)
    required_fields = {
      "alert_context": "primary",
      "alert_dismissible": False,
      "attributes": {},
      "alert_shadow": "",
      "responsive_visibility": None,
      "margin_x": "",
      "margin_y": "",
      "margin_devices": None,
      "padding_x": "",
      "padding_y": "",
      "padding_devices": None,
    }
    return setItem(item, "Alert", field_names, required_fields, {'alert_dismissable': 'alert_dismissible'})
