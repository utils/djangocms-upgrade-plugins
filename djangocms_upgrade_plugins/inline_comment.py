"""
Old:
{
  "model": "cms.cmsplugin",
  "pk": 3774,
  "fields": {
    "path": "0071",
    "depth": 1,
    "numchild": 0,
    "placeholder": 16,
    "parent": null,
    "position": 1,
    "language": "cs",
    "plugin_type": "InlineCommentPlugin",
    "creation_date": "2023-08-28T08:11:36.040Z",
    "changed_date": "2023-08-28T08:11:36.052Z"
  }
},

New:
{
  "model": "cms.cmsplugin",
  "pk": 3774,
  "fields": {
    "path": "0071",
    "depth": 1,
    "numchild": 1,
    "placeholder": 16,
    "parent": null,
    "position": 1,
    "language": "cs",
    "plugin_type": "EditorNotePlugin",
    "creation_date": "2023-08-28T08:01:51.557Z",
    "changed_date": "2023-08-28T08:01:51.561Z"
  }
},
{
  "model": "cms.cmsplugin",
  "pk": 3775,
  "fields": {
    "path": "00710001",
    "depth": 2,
    "numchild": 0,
    "placeholder": 16,
    "parent": 3774,
    "position": 0,
    "language": "cs",
    "plugin_type": "TextPlugin",
    "creation_date": "2023-08-28T08:01:56.714Z",
    "changed_date": "2023-08-28T08:02:05.268Z"
  }
},


Old:
{
  "model": "djangocms_inline_comment.inlinecomment",
  "pk": 3774,
  "fields": {
    "cmsplugin_ptr": 3774,
    "body": "<p>Pozn\u00e1mka pro Editora:</p>\n\n<p>Toto je <strong>pozn\u00e1mka</strong>.</p>"
  }
},

New:
{
  "model": "djangocms_text_ckeditor.text",
  "pk": 3775,
  "fields": {
    "cmsplugin_ptr": 3775,
    "body": "<p>Poznámka editora.</p>"
  }
},
"""
from copy import deepcopy

from .utils import DataDict


def inline_comment(item: DataDict) -> DataDict:
    """Inline Comment content."""
    plugin = deepcopy(item)
    plugin["model"] = "djangocms_text_ckeditor.text"
    return plugin
