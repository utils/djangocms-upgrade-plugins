"""
Old:
{
  "model": "cms.cmsplugin",
  "pk": 746,
  "fields": {
    "path": "0015",
    "depth": 1,
    "numchild": 1,
    "placeholder": 11,
    "parent": null,
    "position": 0,
    "language": "cs",
    "plugin_type": "StylePlugin",
    "creation_date": "2023-08-09T06:43:22.590Z",
    "changed_date": "2023-08-10T07:30:49.506Z"
  }
}
New:
{
  "model": "cms.cmsplugin",
  "pk": 2,
  "fields": {
    "path": "0007",
    "depth": 1,
    "numchild": 1,
    "placeholder": 10,
    "parent": null,
    "position": 1,
    "language": "cs",
    "plugin_type": "GridContainerPlugin",
    "creation_date": "2023-08-14T14:08:05.757Z",
    "changed_date": "2023-08-14T14:08:05.761Z"
  }
}

Old:
{
  "model": "djangocms_style.style",
  "pk": 746,
  "fields": {
    "template": "default",
    "label": "KONTAKT",
    "tag_type": "section",
    "class_name": "",
    "additional_classes": "bg-secondary, pt-8, pt-md-11, pb-11, pb-md-12",
    "id_name": "kontakt",
    "attributes": {},
    "padding_top": null,
    "padding_right": null,
    "padding_bottom": null,
    "padding_left": null,
    "margin_top": null,
    "margin_right": null,
    "margin_bottom": null,
    "margin_left": null,
    "cmsplugin_ptr": 746
  }
}
New:
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 2,
  "fields": {
    "cmsplugin_ptr": 2,
    "ui_item": "GridContainer",
    "tag_type": "div",
    "config": {
      "container_type": "container",
      "attributes": {},
      "plugin_title": {
        "show": false,
        "title": ""
      },
      "background_context": "",
      "background_opacity": "100",
      "background_shadow": "",
      "responsive_visibility": null,
      "margin_x": "",
      "margin_y": "",
      "margin_devices": null,
      "padding_x": "",
      "padding_y": "",
      "padding_devices": null,
      "size_x": "",
      "size_y": ""
    }
  }
}
"""
import re

from .utils import DUMP_UNKNOWN_FIELDS, DataDict, addAttribute, debugFields, fillConfig, setAlignment, setSpaces

DJANGOCMS_FRONTEND_GRID_CONTAINERS = [
    "container",
    "container-fluid",
    "container-full",
    " ",
]


def styleStyle(item: DataDict) -> DataDict:
    """Convert Style."""
    field_names = (
        "container_type",
        "template",
        "label",
        "tag_type",
        "class_name",
        "additional_classes",
        "additional_class_names",
        "id_name",
        "attributes",
        "padding_top",
        "padding_right",
        "padding_bottom",
        "padding_left",
        "margin_top",
        "margin_right",
        "margin_bottom",
        "margin_left",
        "alignment",
    )
    debugFields(item, field_names, disable=not DUMP_UNKNOWN_FIELDS)
    fields = item["fields"]
    class_name = fields["class_name"]
    if not class_name:
        class_name = " "
    if class_name not in DJANGOCMS_FRONTEND_GRID_CONTAINERS:
        class_name = " "
    required_fields = {
        "container_type": class_name,
        "background_context": "",
        "background_opacity": "100",
        "background_shadow": "",
        "responsive_visibility": None,
        "margin_x": "",
        "margin_y": "",
        "margin_devices": None,
        "padding_x": "",
        "padding_y": "",
        "padding_devices": None,
        "size_x": "",
        "size_y": "",
    }
    new_item = {
        "model": "djangocms_frontend.frontenduiitem",
        "pk": item['pk'],
        "fields": {
            "cmsplugin_ptr": item['pk'],
            "ui_item": "GridContainer",
            "tag_type": fields["tag_type"],
        }
    }
    config = fillConfig(item, ["attributes"], required_fields)
    config["plugin_title"] = {
        "show": False,
        "title": fields["label"],
    }
    new_item['fields']['config'] = config
    if fields["class_name"] not in DJANGOCMS_FRONTEND_GRID_CONTAINERS:
        addAttribute(item, new_item, 'class_name', 'class')
    addAttribute(item, new_item, 'additional_classes', 'class',
                 normalize_fnc=lambda value: re.sub(r"\s+", " ", value.replace(",", " ")).strip())
    addAttribute(item, new_item, 'additional_class_names', 'class',
                 normalize_fnc=lambda value: re.sub(r"\s+", " ", value.replace(",", " ")).strip())
    addAttribute(item, new_item, 'id_name', 'id')

    config['margin_x'], config['margin_y'] = setSpaces(fields, 'margin')
    config['padding_x'], config['padding_y'] = setSpaces(fields, 'padding')

    setAlignment(item, new_item)  # Convert left and right to new values start and end.
    return new_item
