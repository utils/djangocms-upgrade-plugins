"""
Old:
{
  "model": "cms.cmsplugin",
  "pk": 2807,
  "fields": {
    "path": "001G0001000100020003",
    "depth": 5,
    "numchild": 0,
    "placeholder": 22,
    "parent": 971,
    "position": 0,
    "language": "cs",
    "plugin_type": "Bootstrap4PicturePlugin",
    "creation_date": "2023-08-11T08:15:01.645Z",
    "changed_date": "2023-08-11T08:15:01.677Z"
  }
}
New:
{
  "model": "cms.cmsplugin",
  "pk": 8,
  "fields": {
    "path": "000A",
    "depth": 1,
    "numchild": 0,
    "placeholder": 10,
    "parent": null,
    "position": 4,
    "language": "cs",
    "plugin_type": "ImagePlugin",
    "creation_date": "2023-08-14T14:38:48.838Z",
    "changed_date": "2023-08-14T14:38:48.845Z"
  }
}

Old:
{
  "model": "bootstrap4_picture.bootstrap4picture",
  "pk": 2807,
  "fields": {
    "template": "default",
    "picture": 49,
    "external_picture": null,
    "width": null,
    "height": null,
    "alignment": "",
    "caption_text": "",
    "attributes": {},
    "link_url": null,
    "link_page": null,
    "link_target": "",
    "link_attributes": {},
    "use_automatic_scaling": true,
    "use_no_cropping": false,
    "use_crop": false,
    "use_upscale": false,
    "use_responsive_image": "inherit",
    "thumbnail_options": null,
    "cmsplugin_ptr": 2807,
    "picture_fluid": false,
    "picture_rounded": false,
    "picture_thumbnail": false
  }
}
New:
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 8,
  "fields": {
    "cmsplugin_ptr": 8,
    "ui_item": "Image",
    "tag_type": "div",
    "config": {
      "template": "default",
      "picture": {
        "model": "filer.image",
        "pk": 1
      },
      "external_picture": "",
      "lazy_loading": false,
      "width": null,
      "height": null,
      "alignment": "start",
      "link_attributes": {},
      "use_automatic_scaling": false,
      "use_crop": false,
      "use_no_cropping": false,
      "use_upscale": false,
      "use_responsive_image": "inherit",
      "thumbnail_options": null,
      "picture_fluid": true,
      "picture_rounded": false,
      "picture_thumbnail": false,
      "attributes": {},
      "external_link": "",
      "internal_link": "",
      "file_link": null,
      "anchor": "",
      "mailto": "",
      "phone": "",
      "target": "",
      "responsive_visibility": null,
      "margin_x": "",
      "margin_y": "",
      "margin_devices": null
    }
  }
}
"""
from .utils import DUMP_UNKNOWN_FIELDS, DataDict, addInternalLink, debugFields, fillPictureConfig


def bootstrap4picture(item: DataDict) -> DataDict:
    """Set plugin item."""
    field_names = (
        "template",
        "external_picture",
        "width",
        "height",
        "alignment",
        "attributes",
        "link_attributes",
        "link_url",
        "link_page",
        "link_target",
        "use_automatic_scaling",
        "use_no_cropping",
        "use_crop",
        "use_upscale",
        "use_responsive_image",
        "thumbnail_options",
        "picture_fluid",
        "picture_rounded",
        "picture_thumbnail",
    )
    debugFields(item, field_names + ("picture",), disable=not DUMP_UNKNOWN_FIELDS)
    new_item = {
        "model": "djangocms_frontend.frontenduiitem",
        "pk": item['pk'],
        "fields": {
            "cmsplugin_ptr": item['pk'],
            "ui_item": "Image",
            "tag_type": "div",
        }
    }
    new_item['fields']['config'] = fillPictureConfig(item, field_names, 'picture', {
        "link_url": "external_link",
        "link_page": "internal_link",
        "link_target": "target",
    })
    addInternalLink(item, new_item)
    return new_item
