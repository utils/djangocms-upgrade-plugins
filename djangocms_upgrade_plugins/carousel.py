"""
Old:
{
    "model": "cms.cmsplugin",
    "pk": 1452,
    "fields": {
        "path": "001O",
        "depth": 1,
        "numchild": 1,
        "placeholder": 18,
        "parent": null,
        "position": 0,
        "language": "cs",
        "plugin_type": "Bootstrap4CarouselPlugin",
        "creation_date": "2023-05-11T13:26:09.392Z",
        "changed_date": "2023-05-11T13:26:09.400Z"
    }
},
{
    "model": "cms.cmsplugin",
    "pk": 1453,
    "fields": {
        "path": "001O0001",
        "depth": 2,
        "numchild": 0,
        "placeholder": 18,
        "parent": 1452,
        "position": 0,
        "language": "cs",
        "plugin_type": "Bootstrap4CarouselSlidePlugin",
        "creation_date": "2023-05-11T13:27:35.974Z",
        "changed_date": "2023-05-11T13:27:36.033Z"
    }
}

{
    "model": "bootstrap4_carousel.bootstrap4carousel",
    "pk": 1452,
    "fields": {
        "cmsplugin_ptr": 1452,
        "template": "default",
        "carousel_interval": 5000,
        "carousel_controls": true,
        "carousel_indicators": true,
        "carousel_keyboard": true,
        "carousel_pause": "hover",
        "carousel_ride": "carousel",
        "carousel_wrap": true,
        "carousel_aspect_ratio": "21x9",
        "tag_type": "div",
        "attributes": {}
    }
},
{
    "model": "bootstrap4_carousel.bootstrap4carouselslide",
    "pk": 1453,
    "fields": {
        "template": "default",
        "name": "",
        "external_link": "",
        "internal_link": null,
        "file_link": null,
        "anchor": "",
        "mailto": "",
        "phone": "",
        "target": "",
        "attributes": {},
        "cmsplugin_ptr": 1453,
        "carousel_image": 1,
        "carousel_content": "<p>Text...</p>",
        "tag_type": "div"
    }
},

New:
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 6,
  "fields": {
    "cmsplugin_ptr": 6,
    "ui_item": "Carousel",
    "tag_type": "div",
    "config": {
      "template": "default",
      "carousel_aspect_ratio": "",
      "carousel_controls": true,
      "carousel_indicators": true,
      "carousel_interval": 5000,
      "carousel_keyboard": true,
      "carousel_pause": "hover",
      "carousel_ride": true,
      "carousel_wrap": true,
      "carousel_transition": "",
      "attributes": {}
    }
  }
},
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 7,
  "fields": {
    "cmsplugin_ptr": 7,
    "ui_item": "CarouselSlide",
    "tag_type": "div",
    "config": {
      "carousel_image": null,
      "carousel_image": {
        "pk": 1,
        "model": "filer.image"
      },
      "carousel_content": "<p>Obsah karuselu.</p>",
      "attributes": {},
      "external_link": "",
      "internal_link": "",
      "file_link": null,
      "anchor": "",
      "mailto": "",
      "phone": "",
      "target": "",
      "background_context": "",
      "background_opacity": "100",
      "background_shadow": ""
    }
  }
}
"""
from .utils import DUMP_UNKNOWN_FIELDS, DataDict, addInternalLink, debugFields, setItem


def bootstrap4carousel(item: DataDict) -> DataDict:
    """Convert bootstrap4carousel."""
    field_names = (
        "template",
        "tag_type",
        "carousel_interval",
        "carousel_controls",
        "carousel_indicators",
        "carousel_keyboard",
        "carousel_pause",
        "carousel_ride",
        "carousel_wrap",
        "carousel_aspect_ratio",
        "attributes",

    )
    debugFields(item, field_names, disable=not DUMP_UNKNOWN_FIELDS)
    required_fields = {
      "template": "default",
      "tab_type": "div",
      "carousel_aspect_ratio": "",
      "carousel_controls": True,
      "carousel_indicators": True,
      "carousel_interval": 5000,
      "carousel_keyboard": True,
      "carousel_pause": "hover",
      "carousel_ride": True,
      "carousel_wrap": True,
      "carousel_transition": "",
      "attributes": {},
    }
    return setItem(item, "CarouselPlugin", field_names, required_fields)


def bootstrap4carousel_slide(item: DataDict) -> DataDict:
    """Convert bootstrap4carouselslide."""
    field_names = (
        "template",
        "tag_type",
        "external_link",
        "internal_link",
        "file_link",
        "anchor",
        "mailto",
        "phone",
        "target",
        "carousel_image",
        "carousel_content",
        "attributes",
    )
    debugFields(item, field_names, disable=not DUMP_UNKNOWN_FIELDS)
    required_fields = {
      "tag_type": "div",
      "carousel_image": None,
      "carousel_content": "",
      "attributes": {},
      "external_link": "",
      "internal_link": "",
      "file_link": None,
      "anchor": "",
      "mailto": "",
      "phone": "",
      "target": "",
      "background_context": "",
      "background_opacity": "100",
      "background_shadow": "",
    }
    new_item = setItem(item, "CarouselSlidePlugin", field_names, required_fields)
    addInternalLink(item, new_item)
    if "carousel_image" in item["fields"]:
        new_item['fields']['config']["carousel_image"] = {
            "model": "filer.image",
            "pk": item["fields"]["carousel_image"]
        }
    return new_item
