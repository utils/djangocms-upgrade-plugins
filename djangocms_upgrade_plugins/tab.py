"""
Old:
{
  "model": "cms.cmsplugin",
  "pk": 1079,
  "fields": {
    "path": "001K0001000200010001",
    "depth": 5,
    "numchild": 2,
    "placeholder": 22,
    "parent": 1078,
    "position": 0,
    "language": "cs",
    "plugin_type": "Bootstrap4TabPlugin",
    "creation_date": "2023-08-09T10:59:28.400Z",
    "changed_date": "2023-08-09T10:59:28.405Z"
  }
}
New:
{
  "model": "cms.cmsplugin",
  "pk": 5,
  "fields": {
    "path": "0008",
    "depth": 1,
    "numchild": 1,
    "placeholder": 10,
    "parent": null,
    "position": 2,
    "language": "cs",
    "plugin_type": "TabPlugin",
    "creation_date": "2023-08-14T14:09:48.326Z",
    "changed_date": "2023-08-14T14:09:48.332Z"
  }
}

Old:
{
  "model": "bootstrap4_tabs.bootstrap4tab",
  "pk": 1079,
  "fields": {
    "cmsplugin_ptr": 1079,
    "template": "default",
    "tab_type": "nav-tabs",
    "tab_alignment": "",
    "tab_index": 1,
    "tab_effect": "",
    "tag_type": "div",
    "attributes": {}
  }
}
New:
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 5,
  "fields": {
    "cmsplugin_ptr": 5,
    "ui_item": "Tab",
    "tag_type": "div",
    "config": {
      "template": "default",
      "tab_type": "nav-tabs",
      "tab_alignment": "",
      "tab_index": null,
      "tab_effect": "",
      "attributes": {}
    }
  }
}

Tab item:

Old:
{
  "model": "cms.cmsplugin",
  "pk": 1080,
  "fields": {
    "path": "001K00010002000100010001",
    "depth": 6,
    "numchild": 4,
    "placeholder": 22,
    "parent": 1079,
    "position": 0,
    "language": "cs",
    "plugin_type": "Bootstrap4TabItemPlugin",
    "creation_date": "2023-08-09T11:02:20.605Z",
    "changed_date": "2023-08-09T11:02:20.614Z"
  }
}
New:
{
  "model": "cms.cmsplugin",
  "pk": 6,
  "fields": {
    "path": "00080001",
    "depth": 2,
    "numchild": 0,
    "placeholder": 10,
    "parent": 5,
    "position": 0,
    "language": "cs",
    "plugin_type": "TabItemPlugin",
    "creation_date": "2023-08-14T14:09:59.851Z",
    "changed_date": "2023-08-14T14:09:59.854Z"
  }
}

Old:
{
  "model": "bootstrap4_tabs.bootstrap4tabitem",
  "pk": 1080,
  "fields": {
    "cmsplugin_ptr": 1080,
    "tab_title": "Tuesday 16 May",
    "tag_type": "div",
    "attributes": {}
  }
}
New:
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 6,
  "fields": {
    "cmsplugin_ptr": 6,
    "ui_item": "TabItem",
    "tag_type": "div",
    "config": {
      "tab_title": "Pokus",
      "tab_bordered": false,
      "attributes": {},
      "padding_x": "",
      "padding_y": "",
      "padding_devices": null
    }
  }
}
"""
from .utils import DUMP_UNKNOWN_FIELDS, DataDict, debugFields, setItem


def bootstrap4tab(item: DataDict) -> DataDict:
    """Convert bootstrap4tab."""
    field_names = (
        "template",
        "tab_type",
        "tab_alignment",
        "tab_effect",
        "tab_index",
        "attributes",
    )
    debugFields(item, field_names + ("tag_type", ), disable=not DUMP_UNKNOWN_FIELDS)
    required_fields: DataDict = {
      "template": "default",
      "tab_type": "nav-tabs",
      "tab_alignment": "",
      "tab_index": None,
      "tab_effect": "",
      "attributes": {}
    }
    return setItem(item, "Tab", field_names, required_fields)


def bootstrap4tab_item(item: DataDict) -> DataDict:
    """Convert bootstrap4tab item."""
    field_names = (
        "tag_type",
        "tab_title",
        "attributes",
    )
    debugFields(item, field_names + ("tag_type", ), disable=not DUMP_UNKNOWN_FIELDS)
    required_fields = {
      "tab_title": "",
      "tab_bordered": False,
      "attributes": {},
      "padding_x": "",
      "padding_y": "",
      "padding_devices": None,
    }
    return setItem(item, "TabItem", field_names, required_fields)
