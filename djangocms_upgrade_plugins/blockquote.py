"""
Old:
{
  "model": "bootstrap4_content.bootstrap4blockquote",
  "pk": 3006,
  "fields": {
    "cmsplugin_ptr": 3006,
    "quote_content": "Block quote text.",
    "quote_origin": "Author",
    "quote_alignment": "text-left",
    "attributes": {}
  }
}

New:
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 4,
  "fields": {
    "cmsplugin_ptr": 4,
    "ui_item": "Blockquote",
    "tag_type": "div",
    "config": {
      "quote_content": "<p>Text of blockquote.</p>",
      "quote_origin": "",
      "quote_alignment": "",
      "attributes": {},
      "margin_x": "",
      "margin_y": "",
      "margin_devices": null,
      "padding_x": "",
      "padding_y": "",
      "padding_devices": null,
      "responsive_visibility": null,
      "background_context": "",
      "background_opacity": "100",
      "background_shadow": ""
    }
  }
}
"""
from .utils import DUMP_UNKNOWN_FIELDS, DataDict, debugFields, setItem


def bootstrap4blockquote(item: DataDict) -> DataDict:
    """Convert Bootstrap 4 Blockquote."""
    field_names = (
        "quote_content",
        "quote_origin",
        "quote_alignment",
        "attributes",
    )
    debugFields(item, field_names, disable=not DUMP_UNKNOWN_FIELDS)
    required_fields = {
      "quote_content": "",
      "quote_origin": "",
      "quote_alignment": "",
      "attributes": {},
      "margin_x": "",
      "margin_y": "",
      "margin_devices": None,
      "padding_x": "",
      "padding_y": "",
      "padding_devices": None,
      "responsive_visibility": None,
      "background_context": "",
      "background_opacity": "100",
      "background_shadow": ""
    }
    new_item = setItem(item, "Blockquote", field_names, required_fields)
    config = new_item["fields"]["config"]
    config["quote_content"] = f"<p>{config['quote_content']}</p>"
    config["quote_origin"] = f"<p>{config['quote_origin']}</p>"
    quote_alignment = {
        "text-left": "start",
        "text-center": "center",
        "text-right": "end",
    }
    config["quote_alignment"] = quote_alignment[config["quote_alignment"]]
    return new_item
