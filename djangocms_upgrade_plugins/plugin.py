from copy import deepcopy
from typing import Sequence

from .utils import DataDict


def setPluginType(item: DataDict, plugin_type: str) -> DataDict:
    """Convert plugin type."""
    new_item = deepcopy(item)
    new_item['fields']["plugin_type"] = plugin_type
    return new_item


def setFieldsPluginType(item: DataDict, plugin_types: Sequence[str]) -> DataDict:
    if item['fields']["plugin_type"] in plugin_types:
        item = setPluginType(item, plugin_types[item['fields']["plugin_type"]])
    return item
