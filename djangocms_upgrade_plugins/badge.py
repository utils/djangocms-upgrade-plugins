"""
Old:
{
  "model": "bootstrap4_badge.bootstrap4badge",
  "pk": 3004,
  "fields": {
    "cmsplugin_ptr": 3004,
    "badge_text": "Badge text (Primary).",
    "badge_context": "primary",
    "badge_pills": false,
    "attributes": {}
  }
}
New:
{
  "model": "djangocms_frontend.frontenduiitem",
  "pk": 2,
  "fields": {
    "cmsplugin_ptr": 2,
    "ui_item": "Badge",
    "tag_type": "div",
    "config": {
      "badge_text": "Primary.",
      "badge_context": "primary",
      "badge_pills": false,
      "attributes": {}
    }
  }
}
"""
from .utils import DUMP_UNKNOWN_FIELDS, DataDict, debugFields, setItem


def bootstrap4badge(item: DataDict) -> DataDict:
    """Convert Bootstrap 4 Badge."""
    field_names = (
        "badge_text",
        "badge_context",
        "badge_pills",
        "attributes",
    )
    debugFields(item, field_names, disable=not DUMP_UNKNOWN_FIELDS)
    required_fields = {
      "badge_text": "",
      "badge_context": "primary",
      "badge_pills": False,
      "attributes": {},
    }
    return setItem(item, "Badge", field_names, required_fields)
