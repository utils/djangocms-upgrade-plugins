# DjangoCMS upgrade plugins

This project is intended to update the [Django CMS QE](https://gitlab.nic.cz/websites/django-cms-qe) project from version [2.2](https://pypi.org/project/django-cms-qe/2.2/) to >= [3.0.0](https://pypi.org/project/django-cms-qe/).
But generally, it can be used to convert plugins from [Django CMS Bootstrap 4](https://github.com/django-cms/djangocms-bootstrap4) == [1.6.0](https://pypi.org/project/djangocms-bootstrap4/1.6.0/)
to [Django CMS Frontend](https://github.com/django-cms/djangocms-frontend) >= [1.1.9](https://pypi.org/project/djangocms-frontend/).

### Install

```
pip install git+https://gitlab.nic.cz/utils/djangocms-upgrade-plugins.git
```


### Usage

The upgrade is performed on data exported from the database into json format. The dump is generated with command `$manager` `dumpdata`, where `$manager` is a Django command, usually `mamange.py`.

Export the data from the database to the original version:

```
$manager dumpdata --natural-primary --natural-foreign --indent 2 --exclude=sessions.session --output my-site.json
```

When converting, you must set the path to the media folder so that the width and length of the images can be set.

Then convert the data to the file `my-site-upgrade.json`:

```
djangocms_upgrade_json_data.py --media=/var/www/my-site/media/ --output my-site-upgrade.json my-site.json
```

After updating your site to the new version of the plugins, delete the original database and create a new one.

```sql
DROP DATABASE my_site;
CREATE DATABASE my_site ... [WITH OWNER ...];
```

Populate the new database with new tables:

```
$manager migrate
```

When migrating, a default record of `example.com` is created in the `django_site` table. This needs to be overwritten with the site that is listed in the json file. There is for example in file `my-site-upgrade.json`:

```json
{
    "model": "sites.site",
    "fields": {
        "domain": "my-site.com",
        "name": "My SITE"
    }
}
```

It is therefore necessary to set the record to:

```sql
UPDATE django_site SET name = 'My SITE', domain = 'my-site.com';
```

If the update had not been done, another site would have been created in addition to `example.com`.

The site data is then uploaded to the database:

```
$manager loaddata my-site-upgrade.json
```

Next, obsolete content types are removed:

```
$manager remove_stale_contenttypes --no-input --include-stale-apps -v3;
```

Finally, a group with permissions for editing frontend plugins can be created:

```
$manager shell < djangocms_editor_permissions.py
```

### Converted plugins

Supported plugins conversion table.

| Old plugin name | New plugin name |
| --------------- | --------------- |
| Bootstrap4AlertsPlugin | AlertPlugin |
| Bootstrap4BadgePlugin | BadgePlugin |
| Bootstrap4BlockquotePlugin | BlockquotePlugin |
| Bootstrap4CardPlugin | CardPlugin |
| Bootstrap4CardInnerPlugin | CardInnerPlugin |
| Bootstrap4CarouselPlugin | CarouselPlugin |
| Bootstrap4CarouselSlidePlugin | CarouselSlidePlugin |
| Bootstrap4CodePlugin | CodePlugin |
| Bootstrap4CollapsePlugin | CollapsePlugin |
| Bootstrap4CollapseContainerPlugin| CollapseContainerPlugin |
| Bootstrap4CollapseTriggerPlugin | CollapseTriggerPlugin |
| Bootstrap4FigurePlugin | FigurePlugin |
| Bootstrap4GridContainerPlugin | GridContainerPlugin |
| Bootstrap4GridRowPlugin | GridRowPlugin |
| Bootstrap4GridColumnPlugin | GridColumnPlugin |
| Bootstrap4JumbotronPlugin | JumbotronPlugin |
| Bootstrap4LinkPlugin | LinkPlugin |
| Bootstrap4SpacingPlugin | SpacingPlugin |
| Bootstrap4TabPlugin | TabPlugin |
| Bootstrap4TabItemPlugin | TabItemPlugin |
| Bootstrap4PicturePlugin | ImagePlugin |
| InlineCommentPlugin | EditorNotePlugin |
| SvgImagePlugin | ImagePlugin |
| StylePlugin | GridContainerPlugin |
| filer.folder | filer.folder |
| djangocms_page_meta.pagemeta | djangocms_page_meta.pagemeta |

> :warning: Warning! Conversion is without warranty. It is not guaranteed that all attributes are converted.

License:
  - [BSD-3-Clause](https://opensource.org/license/bsd-3-clause/)

Authors:
 - Zdeněk Böhm; zdenek.bohm@nic.cz
