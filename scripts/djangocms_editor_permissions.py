#!/usr/bin/env python3
"""
from django.contrib.auth.models import Permission
for perm in Permission.objects.all().order_by('content_type__app_label', 'content_type__model'):
    print(f"{perm.content_type.app_label: <30} {perm.content_type.model: <30} {perm.name: <40} {perm}")
"""
from django.contrib.auth.models import Group, Permission

content_type_app_label = (
    'accordion',
    'alert',
    'badge',
    'card',
    'carousel',
    'collapse',
    'content',
    'grid',
    'image',
    'link',
    'listgroup',
    'tabs',
)

group_name = 'Editor'
editor, created = Group.objects.get_or_create(name=group_name)
if created:
    print(f"Created group '{group_name}'.")

added_permissions = 0
editor_permissions = editor.permissions.all()

for name in content_type_app_label:
    for perm in Permission.objects.filter(content_type__app_label=name):
        if perm in editor_permissions:
            print(' -', perm)
        else:
            print(' +', perm)
            added_permissions += 1
            editor.permissions.add(perm)
print(f"Added permissions: {added_permissions}")

