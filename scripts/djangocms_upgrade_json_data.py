#!/usr/bin/env python3
import argparse
import json
import sys
from copy import deepcopy
from io import TextIOWrapper
from typing import List, Optional

from djangocms_upgrade_plugins.alert import bootstrap4alerts
from djangocms_upgrade_plugins.badge import bootstrap4badge
from djangocms_upgrade_plugins.blockquote import bootstrap4blockquote
from djangocms_upgrade_plugins.card import bootstrap4card, bootstrap4cardinner
from djangocms_upgrade_plugins.carousel import bootstrap4carousel, bootstrap4carousel_slide
from djangocms_upgrade_plugins.code import bootstrap4code
from djangocms_upgrade_plugins.collapse import (bootstrap4collapse, bootstrap4collapse_container,
                                                bootstrap4collapse_trigger)
from djangocms_upgrade_plugins.figure import bootstrap4figure
from djangocms_upgrade_plugins.filer import createImage, filerFile, filerFolder
from djangocms_upgrade_plugins.grid import grid_column_container, grid_container, grid_row_container
from djangocms_upgrade_plugins.inline_comment import inline_comment
from djangocms_upgrade_plugins.jumbotron import bootstrap4jumbotron
from djangocms_upgrade_plugins.link import bootstrap4link
from djangocms_upgrade_plugins.page_meta import page_meta
from djangocms_upgrade_plugins.picture import bootstrap4picture
from djangocms_upgrade_plugins.plugin import setFieldsPluginType
from djangocms_upgrade_plugins.spacing import bootstrap4spacing
from djangocms_upgrade_plugins.style import styleStyle
from djangocms_upgrade_plugins.svg import prepare_for_style_figure, svg_image
from djangocms_upgrade_plugins.tab import bootstrap4tab, bootstrap4tab_item
from djangocms_upgrade_plugins.text_ck_editor import addTextCKEditorPlugin
from djangocms_upgrade_plugins.utils import DataDict, addPluginType, duplicatePlugin

PLUGIN_TYPES = {
    "SvgImagePlugin":                   "ImagePlugin",
    "Bootstrap4GridContainerPlugin":    "GridContainerPlugin",
    "Bootstrap4GridRowPlugin":          "GridRowPlugin",
    "Bootstrap4GridColumnPlugin":       "GridColumnPlugin",
    "Bootstrap4LinkPlugin":             "LinkPlugin",
    "Bootstrap4TabPlugin":              "TabPlugin",
    "Bootstrap4TabItemPlugin":          "TabItemPlugin",
    "Bootstrap4PicturePlugin":          "ImagePlugin",
    "Bootstrap4AlertsPlugin":           "AlertPlugin",
    "Bootstrap4BadgePlugin":            "BadgePlugin",
    "Bootstrap4BlockquotePlugin":       "BlockquotePlugin",
    "Bootstrap4CardPlugin":             "CardPlugin",
    "Bootstrap4CardInnerPlugin":        "CardInnerPlugin",
    "Bootstrap4CodePlugin":             "CodePlugin",
    "Bootstrap4FigurePlugin":           "FigurePlugin",
    "Bootstrap4JumbotronPlugin":        "JumbotronPlugin",
    "Bootstrap4SpacingPlugin":          "SpacingPlugin",
    "StylePlugin":                      "GridContainerPlugin",
    "InlineCommentPlugin":              "EditorNotePlugin",
    "Bootstrap4CollapsePlugin":         "CollapsePlugin",
    "Bootstrap4CollapseContainerPlugin": "CollapseContainerPlugin",
    "Bootstrap4CollapseTriggerPlugin":  "CollapseTriggerPlugin",
    "Bootstrap4CarouselPlugin":         "CarouselPlugin",
    "Bootstrap4CarouselSlidePlugin":    "CarouselSlidePlugin",
    "EmailIntoFromField":               "EmailField",
    "OIDCEmailIntoFromField":           "OIDCEmailField",
}
CONVERT_MODEL = {
    "cmsplugin_svg.svgimage": svg_image,
    "bootstrap4_grid.bootstrap4gridcontainer": grid_container,
    "bootstrap4_grid.bootstrap4gridrow": grid_row_container,
    "bootstrap4_grid.bootstrap4gridcolumn": grid_column_container,
    "bootstrap4_link.bootstrap4link": bootstrap4link,
    "bootstrap4_tabs.bootstrap4tab": bootstrap4tab,
    "bootstrap4_tabs.bootstrap4tabitem": bootstrap4tab_item,
    "bootstrap4_picture.bootstrap4picture": bootstrap4picture,
    "bootstrap4_alerts.bootstrap4alerts": bootstrap4alerts,
    "bootstrap4_badge.bootstrap4badge": bootstrap4badge,
    "bootstrap4_content.bootstrap4blockquote": bootstrap4blockquote,
    "bootstrap4_card.bootstrap4card": bootstrap4card,
    "bootstrap4_card.bootstrap4cardinner": bootstrap4cardinner,
    "bootstrap4_content.bootstrap4code": bootstrap4code,
    "bootstrap4_content.bootstrap4figure": bootstrap4figure,
    "bootstrap4_jumbotron.bootstrap4jumbotron": bootstrap4jumbotron,
    "bootstrap4_utilities.bootstrap4spacing": bootstrap4spacing,
    "filer.folder": filerFolder,
    "filer.file": filerFile,
    "djangocms_style.style": styleStyle,
    "djangocms_inline_comment.inlinecomment": inline_comment,
    "bootstrap4_collapse.bootstrap4collapse": bootstrap4collapse,
    "bootstrap4_collapse.bootstrap4collapsecontainer": bootstrap4collapse_container,
    "bootstrap4_collapse.bootstrap4collapsetrigger": bootstrap4collapse_trigger,
    "bootstrap4_carousel.bootstrap4carousel": bootstrap4carousel,
    "bootstrap4_carousel.bootstrap4carouselslide": bootstrap4carousel_slide,
    "djangocms_page_meta.pagemeta": page_meta,
}


def _trim_permissions(current_permissions: List[List[str]]) -> List[List[str]]:
    """Remove unused permissions."""
    permissions = []
    obsolete_perms = (
        'svgimage',
        'inlinecomment',
        'link',
        'style',
        'filerfile',
        'filerfolder',
        'filerimage',
        'filerlinkplugin',
        'matalog',
    )
    for perms in current_permissions:
        if 'bootstrap4' in perms[1] or 'bootstrap3' in perms[1] or perms[2] in obsolete_perms \
                or 'cms_qe_auth' in perms[1]:
            continue
        permissions.append(perms)
    return permissions


def _new_auth_group(item: DataDict) -> DataDict:
    """Create new auth.group."""
    return {
        'model': 'auth.group',
        'fields': {
            'name': item['fields'].pop('group_ptr')[0],
            'permissions': _trim_permissions(item['fields']['permissions']),
        }
    }


def convert(source: DataDict, output: TextIOWrapper, media_root: Optional[str]) -> None:
    """Convert source and write to the output."""
    images: List[int] = [item['fields']['file_ptr'] for item in source if item['model'] == 'filer.image']
    image_id: int = max([item['pk'] for item in source if item['model'] == 'filer.image'] + [0])
    cms_plugin_id: int = max([item['pk'] for item in source if item['model'] == 'cms.cmsplugin'])
    comment_content = {}
    svg_content = {}
    new_data = []
    for item in source:
        if item['model'] == "auth.group":
            continue
        if item['model'] == "cms_qe_auth.group":
            new_item = _new_auth_group(item)
        elif item['model'] == "cms_qe_auth.user":
            new_item = deepcopy(item)
            new_item["fields"]["user_permissions"] = _trim_permissions(item['fields']['user_permissions'])
        elif item['model'] == "cms.cmsplugin":
            new_item = setFieldsPluginType(item, PLUGIN_TYPES)
            if new_item["fields"]['plugin_type'] == "EditorNotePlugin":
                new_data.append(new_item)
                cms_plugin_id += 1
                comment_content[new_item["pk"]] = cms_plugin_id
                new_item = addPluginType(new_item, cms_plugin_id, 'TextPlugin')
            if item["fields"]['plugin_type'] == "SvgImagePlugin":
                svg_content[item["pk"]] = new_item
        else:
            new_item = CONVERT_MODEL.get(item['model'], lambda obj: obj)(item)
            if new_item['model'] == "filer.file":
                if new_item["fields"]["polymorphic_ctype"] == ["filer", "image"]:
                    if new_item['pk'] not in images:
                        image_id += 1
                        image_item = createImage(new_item, image_id, new_item['pk'], media_root)
                        new_data.append(image_item)
                        images.append(new_item['pk'])
            elif item['model'] == "djangocms_inline_comment.inlinecomment":
                new_item["pk"] = comment_content[item['pk']]
                new_item["fields"]["cmsplugin_ptr"] = comment_content[item['pk']]
            elif item['model'] == "cmsplugin_svg.svgimage" and item['fields']['tag_type']:
                cms_plugin_id += 1  # figure
                figure = duplicatePlugin(svg_content[item['pk']], cms_plugin_id, "GridContainerPlugin")
                figure['fields']['numchild'] += 1
                new_data.append(figure)
                new_data.append(styleStyle(prepare_for_style_figure(item, cms_plugin_id)))
                # Move SVG image under the element figure.
                svg_content[item["pk"]]["fields"]["parent"] = cms_plugin_id
                svg_content[item["pk"]]["fields"]["path"] = figure["fields"]["path"] + "0001"
                svg_content[item["pk"]]["fields"]["depth"] += 1
                svg_content[item["pk"]]["fields"]["position"] = 0
                # Add element figurecaption.
                if item['fields']['caption_text']:
                    cms_plugin_id += 1  # figurecaption
                    figure_caption = addPluginType(figure, cms_plugin_id, 'GridContainerPlugin', "0002", 1)
                    new_data.append(figure_caption)
                    new_data.append(styleStyle(prepare_for_style_figure(item, cms_plugin_id, 'figurecaption')))
                    cms_plugin_id += 1  # text of figurecaption
                    new_data.append(addPluginType(figure_caption, cms_plugin_id, 'TextPlugin'))
                    caption_text = item['fields']['caption_text']
                    new_data.append(addTextCKEditorPlugin(cms_plugin_id, f'<div>{caption_text}</div>'))
        new_data.append(new_item)
    json.dump(new_data, output, indent=4)


def main():
    """Run conversion."""
    parser = argparse.ArgumentParser()
    parser.add_argument('source')
    parser.add_argument('-o', '--output')
    parser.add_argument('-m', '--media', help='Folder name of the media root.')

    args = parser.parse_args()

    with open(args.source) as handle:
        data = json.load(handle)
    if args.output:
        with open(args.output, 'w') as handle:
            convert(data, handle, args.media)
    else:
        convert(data, sys.stdout, args.media)


main()
